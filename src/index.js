import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Login from './component/Login/Login';


ReactDOM.render(
  <React.StrictMode>
    <App/>
  </React.StrictMode>,
  document.getElementById('root')
);

