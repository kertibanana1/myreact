import './App.css';
import React, {useState} from 'react';
import {Input , Button,  Table , Popconfirm, Form ,InputNumber, Typography, Row, Col } from 'antd';
import "antd/dist/antd.css";

const EditableCell = ({
  editing,
  dataIndex,
  title,
  inputType,
  record,
  index,
  children,
  ...restProps
}) => {
  const inputNode = inputType === 'number' ? <InputNumber /> : <Input />;
  return (
    <td {...restProps}>
      {editing ? (
        <Form.Item
          name={dataIndex}
          style={{
            margin: 0,
          }}
         >
          {inputNode}
        </Form.Item>
      ) : (
        children
      )}
    </td>
  );
};

var key = 3;

function App() {
  
  const arr = [
    {
        key: 1,
        registerId : 'จก1992',
        brand : 'honda',
        model : 'civic',
        note  : '-'
    },
    {
        key: 2,
        registerId : 'จก1546',
        brand : 'honda',
        model : 'city',
        note  : '-'
    },
    
    ];

  const [registerId, setRegisterId] = useState('');
  const [model, setModel] = useState('');
  const [brand, setBrand] = useState('');
  const [note, setNote] = useState('');
  const [dataSource, setDataSource] = useState(arr);

  const [form] = Form.useForm();
  const [editingKey, setEditingKey] = useState('');

  const isEditing = (record) => record.key === editingKey;

  const edit = (record) => {
    form.setFieldsValue({
      registerId: '',
      brand: '',
      model: '',
      note: '',
      ...record,
    });
    setEditingKey(record.key);
  };

  const cancel = () => {
    setEditingKey('');
  };

  const save = async (key) => {
    try {
      const row = await form.validateFields();
      const newData = [...dataSource];
      const index = newData.findIndex((item) => key === item.key);

      if (index > -1) {
        const item = newData[index];
        newData.splice(index, 1, { ...item, ...row });
        setDataSource(newData);
        setEditingKey('');
      } else {
        newData.push(row);
        setDataSource(newData);
        setEditingKey('');
      }
    } catch (errInfo) {
      console.log('Validate Failed:', errInfo);
    }
  };


  function handleDelete(key) {
      console.log(key);
      const data_state = [...dataSource];
      setDataSource(data_state.filter((item) => item.key !== key));
    }
  

   function submit() {
        
        if ((model && registerId && brand ) === '') {
            alert('กรอกด้วย')
            return
        }
        key = key + 1;

        const obj = {
          key: key,
          registerId : registerId,
          brand : brand,
          model : model,
          note  : note
        }


        // data_obj.id = state.length + 1
        const newState = [...dataSource];
        newState.push(obj);
        setDataSource(newState);
       
        setNote('')
        setModel('')
        setRegisterId('')
        setBrand('')

   }

   const columns = [
    {
      title: 'RegisterId',
      dataIndex: 'registerId',
      key: 'registerId',
      editable: true,
    },   
    {
      title: 'Brand',
      dataIndex: 'brand',
      key: 'brand',
      editable: true,
    },
    {
      title: 'Model',
      dataIndex: 'model',
      key: 'model',
      editable: true,
    },
    {
      title: 'Note',
      key: 'note',
      dataIndex: 'note',
      editable: true,
    },
    {
      title: 'Delete',
      key: 'delete',
      render: (_, record) => 
           dataSource.length >= 1 ? (
            <Popconfirm title="Sure to delete?" onConfirm={() => handleDelete(record.key)}>
              <a>Delete</a>
            </Popconfirm>
          ) : null,
    },
    {
      title: 'Edit',
      dataIndex: 'edit',
      render: (_, record) => {
        // eslint-disable-next-line no-undef
        const editable = isEditing(record);
        return editable ? (
          <span>
            <a
              href='#'
              onClick={() => save(record.key)}
              style={{
                marginRight: 8,
              }}
            >
              Save
            </a>
            <Popconfirm title="Sure to cancel?" onConfirm={cancel}>
              <a>Cancel</a>
            </Popconfirm>
          </span>
        ) : (
          <Typography.Link disabled={editingKey !== ''} onClick={() => edit(record)}>
            Edit
          </Typography.Link>
        );
      },
    },
  ];

  const mergedColumns = columns.map((col) => {
    if (!col.editable) {
      return col;
    }

    return {
      ...col,
      onCell: (record) => ({
        record,
        inputType: col.dataIndex,
        dataIndex: col.dataIndex,
        title: col.title,
        editing: isEditing(record),
      }),
    };
  });

  return (
    
    <div className="App">
       <div className="Insert">
            <div className="Insert_input" >
            <Row>
                <Col span={2}>ทะเบียนรถ :</Col>
                <Col span={22}>
                  <Input 
                    className="Insert_input_registerId" 
                    name="registerId"  
                    value={registerId}
                    onChange={e => setRegisterId(e.target.value)}
                    >
                  </Input>
                </Col>
            </Row>
            <br></br>
            <Row>
              <Col span={2}>ยี่ห้อ : </Col>
              <Col span={22}>
                  <Input
                  className="Insert_input_brand" 
                  name="brand"
                  value={brand}
                  onChange={e => setBrand(e.target.value)}
                  >
                  </Input>
              </Col>
            </Row> 
            <br></br>
            <Row>
              <Col span={2}>รุ่นรถ :</Col>
              <Col span={22}>
                  <Input
                      className="Insert_input_model" 
                      name="model"
                      value={model}
                      onChange={e => setModel(e.target.value)}
                      >
                  </Input>
              </Col>
            </Row> 
            <br></br>  
            <Row>
                <Col span={2}>หมายเหตุ : </Col>
                <Col span={22}> 
                <Input 
                    className="Insert_input_note" 
                    name="note"
                    value={note}
                    onChange={e => setNote(e.target.value)}
                    >
                </Input>
               </Col>
            </Row>
            <br></br>
               <Button className="Button_summit" type="primary" onClick={submit}>
                 เพิ่ม
               </Button>
            </div>
        </div>
     <br></br>
     <Form form={form} component={false}>
      <Table 
       components={{
        body: {
          cell: EditableCell,
        },
      }}
      columns={mergedColumns}
      bordered
      dataSource={dataSource} />

      </Form>
       {/* <Table border="1">
       <tbody>
        <tr>
          <th>ทะเบียนรถ</th>
          <th>ยี่ห้อ</th>
          <th>รุ่น</th>
          <th>โน้ต</th>
          <th>ลบ</th>
        </tr>
        {state.map((item, index) => (  
          <tr key={item.id}>
            <td >{item.registerId}</td>
            <td>{item.brand}</td>
            <td>{item.model}</td>
            <td>{item.note}</td>
            <td>
            </td>
          </tr>
          ))}  
        </tbody>
      </Table> */}

    </div>
    
  );

  
}

export default App;


