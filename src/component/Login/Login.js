import React ,{ Component }from 'react';
import {Form, Input, Button, Card } from 'antd';
import './Login.css'
import App from './../../App'
import { Route } from 'react-router-dom'

const Home = () => <h1>Home</h1>
const About = () => <h1>About</h1>
const Post = () => <h1>Post</h1>
const Project = () => <h1>Project</h1>


function Login (){

    const submitTrued = (values) => {
        console.log('Success:', values);
        window.location.href = '/'     
    };
    
    const submitFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };


    return(
    <Card className="Login" style={{ width: 500}}>
    <Form
      name="basic"
        initialValues={{
          remember: true,
        }}
        onFinish={submitTrued}
        onFinishFailed={submitFailed}
        autoComplete="off"
      >
      <Form.Item
        label="Username"
        name="username"
        rules={[
          {
            required: true,
            message: 'Please input your username!',
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Password"
        name="password"
        rules={[
          {
            required: true,
            message: 'Please input your password!',
          },
        ]}
      >
        <Input.Password />
      </Form.Item>
      <Form.Item>
        <Button className="Button_summit" type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
   
    </Card>
    
  )
}

export default Login;